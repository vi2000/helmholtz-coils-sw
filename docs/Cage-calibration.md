[TOC]

# Helmholtz Cage calibrbation procedure development


The goal of the calibrbation procedure is to map the magnetic field to the duty cycle.

## Initial approach

The calibrating procedure is initiated by conducting a software-guided alignment of the magnetometer on a single axis. This technique is based on the assumption that the magnetic field vectors produced are both orthogonal and linearly independent. The magnetometer is positioned approximately at the center of the cage. The coils are powered up with a voltage with a 100% duty cycle, as this will amplify the generated magnetic field and simplify the identification of the observed differences.
Subsequently, a cyclic activation and deactivation of the power of the coils for a single channel are initiated, while simultaneously observing the magnetometer's readings. The objective is to induce substantial alteration along a single axis while maintaining consistency along the remaining axes. This process should continue until the observed changes on the theoretically non-affected axes are within a predetermined tolerance. 

Upon completion of the position calibration, measurements are to be taken across a range of duty cycles spanning from 0% to 100%, encompassing both forward and reverse current scenarios. These measurements are subsequently to be correlated with the corresponding observed magnetic field values.

Following the collection of data, an interpolation process should be conducted, which involves the derivation of a function using the acquired dataset, employing a line-fitting technique to establish a mathematical representation of the relationship between the measured parameters.

## New calibration strategy 

During the execution of the previously described calibration procedure, observations were made indicating that the behavior exhibited signifies that the magnetic field produced by each set of coils is not impeccably orthogonal in relation to others. This phenomenon gives rise to significant accuracy discrepancies if left unaddressed.

Moreover, due to the discoveries made in the issue [#76](https://gitlab.com/librespacefoundation/helmholtz-coils-hw/-/issues/76), we developed a new approach for the calibration procedure for the Helmholtz Cage. This procedure  considers the current's reliance on various channels and the less-than-perfect orthogonal arrangement of the cage setup.

---

The beginning of the new calibration procedure involves the placement of the magnetometer at the geometric center of the system, instead of solely relying on software-driven positioning. Once the magnetometer's precise placement has been performed, the system's axes are defined and documented in accordance with those of the magnetometer.

Following this, a process of data logging ensues, where magnetic field along the x, y, and z axes are recorded for a range of six distinct duty cycle values. This range is varying from 0% to 100%, encompassing both forward and reverse current scenarios.

In order to effectively group and organize these data points, consideration is be given to the utilization of a machine learning technique to map the magnetic field to duty cycle data.

### Calculating the number of possible combinations

To state the problem, you are working with a magnetic field vector $M = [M_x, M_y, M_z]$, the components of which are reliant upon a set of six duty cycle values $d = [d_1, d_2, d_3, d_4, d_5, d_6]$. This grouping of duty cycles spans an interval from 0 to 1, with increments of 0.04. Specifically, $d_1$ and $d_2$ are linked, signifying that as one value changes, the other remains fixed at 1. This same relationship applies to $d_3$ and $d_4$, as well as $d_5$ and $d_6$. 

Then, to calculate the number of possible combinations of the magnetic field vector for each duty cycle, the following equation is used:

```math
\text{Combinations} = \frac{\text{Range}}{\text{Step size}} + 1 = \frac{1}{0.04} + 1 = 26
```

If we assume an intention to calculate all possible solutions for $d_2 = 1$, four distinct cases emerge for the duty cycles $d_3$, $d_4$, $d_5$, and $d_6$. The table below outlines these scenarios:

|  Case  |  $d_3$  |  $d_4$  |  $d_5$  |  $d_6$  |
|:-------:|:-------:|:-------:|:-------:|:-------:|
|      1  |    1    |$[0,...,1]$|    1    |$[0,...,1]$|
|      2  |    1    |$[0,...,1]$|$[0,...,1]$|    1    |
|      3  |$[0,...,1]$|    1    |    1    |$[0,...,1]$|
|      4  |$[0,...,1]$|    1    |$[0,...,1]$|    1    |



If our aim is to compute all feasible combinations, we must also incorporate the combinations derived from $d_1 = 1$, which is the same as the number of cases outlined earlier.

Finally, the total number of possible combinations can be calculated as:

```math
\text{Total combinations} = \text{Number of choices}^6 = 8\cdot26^3 = 140{,}608
```

Hence, there are $1.4\cdot10^{5}$ different possible combinations for the $M$ vector based on the given duty cycle ranges.


### Setting maximum update rate on the RM3100

First, to achieve a Maximum Single-Axis Sample Rate of 850 Hz, the cycle count should be set to 100. Then, the time between measurements in Continuous Measurement Mode is established by utilizing the TMRC register. Refer to [p. 31 of the datasheet](https://www.pnicorp.com/wp-content/uploads/RM3100-RM2100-Sensor-Suite-User-Manual-V9.0.pdf). The TMRC value of 92 (Hex) should be used to achieve an approximate time of 1.7 ms between readings, resulting in an update rate of around 600 Hz. Due to the approximate nature of update rates, and in consideration of a deviation tolerance of 7%, the delay should be set to 2 ms in the code. Since the maximum data rate of the sensor is set to 850 Hz by the CCR register, exceeding the rate defined by the TMRC register, no overriding of the sample frequency should occur.


### Run-time estimation

The equation used to compute the time in minutes is provided below:

```math
\text{time} = \frac{\text{combinations} \cdot 2.04167 ms \cdot \text{repeated\_measurements}}{1000 \cdot 60 } \, [\text{minutes}]
```

- $\text{repeated\_measurements}$: the number of measurements to take for the same set of duty cycles
- $\text{combinations}$: the total number of possible combinations of the magnetic field vector


The 6 ms time interval refers to the estimated duration taken for acquiring and transmitting a signal value from the magnetometer, as well as implementing the duty cycle. To elaborate, the subsequent measurement becomes accessible approximately 1.75 ms ± 0.1225 ms after the previous one. Consequently, once the UART transmission, lasting around 5.2 ms([calculation](https://gitlab.com/librespacefoundation/helmholtz-coils-hw/-/issues/66#note_1509376168)), is completed, the value will be readily available. Thus, there is no requirement for the utilization of the HAL_Delay() function. The value is extended to 6 ms to encompass the processes of obtaining magnetometer data and executing the duty cycle imposing routines.

In our scenario, the calculated time for calibration stands at 42.1824 minutes, a duration that has been verified through testing. The parameters are presented below.


### Small code snippet for a quick calculation

<details><summary>Click to expand</summary>

```
range = 100;
step = 4;


repeated_measurements = 3;

choices = range/step + 1;
combinations = 8*choices^3;
time = combinations*6*repeated_measurements/1000/60 % mins

```

</details>


### Calculating UART transmission speed 

#### Calculation of bits within the transmitted string

The distribution of bits in the transmitted string format, `"Completion %f %%, %f, %f, %f, %f, %f, %f, %f, %f, %f \r\n"` is as follows:

Here's how the bits are distributed:
- "Completion" is a string of characters. The exact number of bits used to represent each character depends on the character encoding being used. For ASCII, each character typically requires 8 bits (1 byte). So, "Completion" would be 10 characters * 8 bits/character = 80 bits.
- The literal "%%" represents a percentage sign and accounts for 16 bits (2 bytes).
- All "%f" placeholders correspond to floating-point values, with each "%f" requiring 4 bytes (32 bits).
- The characters " ", "\r", "\n," and "," each necessitate 8 bits (1 byte).

Adding everything up:

```math
\begin{equation*}
\begin{aligned}
\text{Number of bits} &= 10 \times 8 \text{ bits ("Completion")} \\
&+ 10 \times 32 \text{ bits (All "\%f" numbers)} \\
&+ 23 \times 8 \text{ bits} \text{ (" ", "\textbackslash r", "\textbackslash n", and "," characters)} \\
&+ 16 \text{ bits} \text{ (The percentage sign "\%")} \\
&= 600 \text{ bits}
\end{aligned}
\end{equation*}
```
#### Baud-rate selection

A baud-rate of 115200 bits per second will be utilized.

To determine if this specific baud rate is supported by your PC, you can employ the following command:

```
stty -F /dev/ttyACM0 115200 && echo Ok.
```

To determine the supported a number of baud rates for your PC, you can employ the following bash code snippet:

```
for bauds in $(
    sed -r 's/^#define\s+B([1-9][0-9]+)\s+.*/\1/p;d' < \
        /usr/include/asm-generic/termbits.h ) ;do
    echo $bauds
    stty -F /dev/ttyACM0 $bauds && echo Ok.
done  2>&1 |
    pr -at2
```

#### Estimation of transmission time

The transmission time for UART communication is influenced by various factors, such as the baud rate, the quantity of bits within each data frame, and any additional overhead introduced by start and stop bits.

The formula to calculate the transmission time for UART is:

```math
\text{Transmission Time} = \frac{\text{Number of bits}}{\text{Baud Rate}} \cdot 1000 
```

Where:

- Number of bits: This includes the data bits, start bit, and stop bit(s).
- Baud Rate: The communication speed in bits per second (bps).

It should be noted that this formula provides a fundamental approximation and does not encompass considerations like synchronization, bit-level timing, and other protocol-specific factors. Furthermore, if there are multiple bytes involved, you will need to account for inter-byte gaps and the transmission time for each byte.

> It appears that while the UART communication's 576000 bits per second rate was supported by my computer, errors were observed in the data, and an unusual behavior was noted. 
The absence of noise in the sensor's output could not be accounted for by the information provided in the [p. 5 of the datasheet](https://www.pnicorp.com/wp-content/uploads/RM3100-RM2100-Sensor-Suite-User-Manual-V9.0.pdf). The expected noise was set at 0.02uT. No variation was detected even up to 0.00001uT. As a result, the baud-rate was altered to 115200, and indications of normal behavior were observed in the communication.

Final transmission time calculation:

```math
\text{Transmission Time} = \frac{\text{Number of bits}}{\text{Baud Rate}} \cdot 1000 = \frac{\text{600}}{\text{115200}} \cdot 1000 = 5.208 ms
```
