from skyfield.api import EarthSatellite, load, wgs84
import igrf
from datetime import timedelta
import argparse
import csv
import numpy as np
import serial
import tensorflow as tf
import time

# igrf.base.build() #Run if the error "FileNotFoundError: [Errno 2] No such file or directory: ../igrf/src/igrf/igrf13_driver" occurs (as referred here https://github.com/space-physics/igrf/issues/13)

class MagneticFieldSimulator:

	def __init__(self):
		self.earths_magetic_field = [1.40553665, 28.6010971,37.3134041]
		self.duty_cycle_array = np.empty((0, 6), dtype=np.float64)
		self.mag_field_array = np.empty((0, 3), dtype=np.float64)
		self.parser = argparse.ArgumentParser()
		self._initialize_arguments()
		self.args = self.parser.parse_args()
		self.initialise_model()

	def _initialize_arguments(self):
		self.parser.add_argument('-json_file', default='Software/Helmholtz-Cage-Calibration/model.json')
		self.parser.add_argument('-weights', default='Software/Helmholtz-Cage-Calibration/model.h5')
		self.parser.add_argument('-ser_port', default='/dev/ttyACM0')
		self.parser.add_argument('-ser_baud', default=115200, type=int)


	def initialise_model(self):
		self.duty_cycle_array = np.empty((0, 6), dtype=np.float64)
		with open(self.args.json_file, 'r') as json_file:
			loaded_model_json = json_file.read()
		self.loaded_model = tf.keras.models.model_from_json(loaded_model_json)
		self.loaded_model.load_weights(self.args.weights)



	def mag2duty(self):
		for magnetic_field_x in range(-100, 126, 25):
			for magnetic_field_y in range(-100, 126, 25):
				for magnetic_field_z in range(-100, 126, 25):
					""" 
					x		y		z
					172.15054	200.27728	210.97691 ULTIMATE MAX
					-165.98526	-143.27615	-140.83955 ULTIMATE MIN
					"""
					mag_field = [magnetic_field_x,magnetic_field_y,magnetic_field_z]
					self.mag_field_array = np.vstack((self.mag_field_array, mag_field))
			
		self.duty_cycle_array = self.loaded_model.predict(np.array(self.mag_field_array))


	def normalization(self):
		self.norm_duty_cycle_array = np.empty((0, 6), dtype=np.float64)
		for row in range(self.duty_cycle_array.shape[0]):
			norm_duty_cycle_array_row = self.duty_cycle_array[row][:]
			
			
			if (self.earths_magetic_field[2] - self.mag_field_array[row][2] < 0): #positive magnetic field generation
				norm_duty_cycle_array_row[1] = 1
			elif (self.earths_magetic_field[2] - self.mag_field_array[row][2] > 0):
				norm_duty_cycle_array_row[0] = 1
			else:
				norm_duty_cycle_array_row[0] = 1
				norm_duty_cycle_array_row[1] = 1
		
			if (self.earths_magetic_field[1] - self.mag_field_array[row][1] < 0):
				norm_duty_cycle_array_row[2] = 1
			elif (self.earths_magetic_field[1] - self.mag_field_array[row][1] > 0):
				norm_duty_cycle_array_row[3] = 1			
			else:
				norm_duty_cycle_array_row[2] = 1
				norm_duty_cycle_array_row[3] = 1
			
			if (self.earths_magetic_field[0] - self.mag_field_array[row][0] < 0):
				norm_duty_cycle_array_row[5] = 1
			elif (self.earths_magetic_field[0] - self.mag_field_array[row][0] > 0):	
				norm_duty_cycle_array_row[4] = 1
			else:
				norm_duty_cycle_array_row[4] = 1
				norm_duty_cycle_array_row[5] = 1
						
			self.norm_duty_cycle_array = np.vstack((self.norm_duty_cycle_array, norm_duty_cycle_array_row))


	def UART_transmission(self):
		print(f"--------------------------------------------")
		print(f"The required calculations have been performed. Initiating the transmission of control inputs through UART to the STM32...")

		# self.norm_duty_cycle_array = np.array([6,5,4,3,2,1])
		serial_port = serial.Serial(self.args.ser_port, 
			      baudrate=self.args.ser_baud, 
				  timeout=2,
				  parity=serial.PARITY_NONE,
				  stopbits=serial.STOPBITS_ONE,
				  bytesize=serial.EIGHTBITS,
				  xonxoff = True,
				  rtscts=False)

		serial_port_RX = serial.Serial('/dev/ttyACM0', 
			baudrate=self.args.ser_baud, 
			timeout=2,
			parity=serial.PARITY_NONE,
			stopbits=serial.STOPBITS_ONE,
			bytesize=serial.EIGHTBITS)
			
		with open("error_log_2.txt", "w") as error_log_file:

			try:
				for i in range(self.norm_duty_cycle_array.shape[0]):
					for _ in range(3):
						formatted_values = []
						for value in self.norm_duty_cycle_array[i]:
							formatted_values.append(f'{value:.2f}')
						values_string = ','.join(formatted_values)
						values_bytes = values_string.encode('utf-8')
						try:
							serial_port.write(values_bytes)
							Progress = i / self.norm_duty_cycle_array.shape[0] * 100
							print(f"Progress:{Progress/100:.2f}/1		Transmitted: {values_string}")
			
							data_in = ""
							data_in = serial_port_RX.read(size = 800).decode() # if the number of data is known, set timeout=None when defining the Serial class
							serial_port_RX.flushInput()
							print("Received mag field :  %s"%(data_in.strip()))
							print("Commanded mag field : %s %s %s"%(self.mag_field_array[i,0],self.mag_field_array[i,1],self.mag_field_array[i,2]))
							print("---------------------------")
							#print( data_in.strip().replace(",","").split())
							values = data_in.strip().replace(",","")
														
							received_mag = [float(values.split()[0]),float(values.split()[1]) ,float(values.split()[2])]
							error_x = abs(received_mag[0] - self.mag_field_array[i,0])
							error_y = abs(received_mag[1] - self.mag_field_array[i,1])
							error_z = abs(received_mag[2] - self.mag_field_array[i,2])
							print(f"Error: {error_x},{error_y},{error_z}, \n")
							error_log_file.write(f"Error: {error_x},{error_y},{error_z}, \n")
							#print(received_mag)

							print("Commanded mag field : %s %s %s"%(self.mag_field_array[i,0],self.mag_field_array[i,1],self.mag_field_array[i,2]))
							#print("---------------------------")
							#error_x = abs(self.mag_field_array[i,0] - )
							# print(f"Transmitted: {self.mag_field_array[i,:]}")
							#print(self.mag_field_array[i,:])
							

						except KeyboardInterrupt: 
							pass
						time.sleep(0.5)
			finally:
				serial_port.close()
				print("Serial port closed.")

if __name__ == "__main__":
	simulator = MagneticFieldSimulator()
	simulator.mag2duty()
	simulator.normalization()
	simulator.UART_transmission()
	print("Finished!")
	
