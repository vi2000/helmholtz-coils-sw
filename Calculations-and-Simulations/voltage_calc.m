function [V,R] = voltage_calc(N,a,d,I)
rho = 1.62e-8; # The resistivity of the copper wire
L = a * 4 * N ; # m
A = pi*d^2/4; # m^2
R = 2*rho*L/A; # Ohm
R = 38;
V = I*R; # Ohm's Law
end
