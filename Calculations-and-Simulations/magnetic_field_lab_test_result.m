## Real Data 

##28V, 0.72A, 98.94uT, 0.9894 Gauss 
##20V, 0.51A, 71.11uT, 0.7111 Gauss 
##12V, 0.30A, 42.86uT, 0.4286 Gauss 
##10V, 0.25A, 36.00uT, 0.3600 Gauss 
##8V,  0.20A, 29.20uT, 0.2920 Gauss 
##6V,  0.14A, 21.56uT, 0.2156 Gauss 


## The desirable range is 2.56 GAuss ~ 3 Gauss
# The real spacing between coils is 44 cm, but the spacing parameter was tuner to correspond to the real data
spacing = 0.528;

## Constants
mu_0 = 1.25663706e-7; # m kg s-2 A-2 - the magnetic constant

## Our Desing Paramenters
side = 0.796; #  meters
turns_in_coil = 80; #Turns
#current_in_coil = 0.3; # Amps
current_in_coil = 0.3 ; # Amps
d = 0.5106e-3; # m (24AWG)

# Calculations
gamma = spacing/side;
q = spacing;
a = side;
# optimal gamma = 0.5445 according to bibliography
x = linspace(-1,1,1000);
side_inch = 39.37* side;

#field_intensity_eqn_1 = 2*4.95e-5*turns_in_coil*current_in_coil/ (pi*side_inch/2) *2/((1+gamma^2)*sqrt(2+gamma^2))*10000;
#field_intensity_eqn_2 = 100000 * mu_0*current_in_coil*turns_in_coil*a^2 / (2*pi) * ( 2 / ( (a^2/4 + q.^2/4) * sqrt(a^2/2 +q.^2/4))); # Gauss
field_intensity = 100000 * mu_0*current_in_coil*turns_in_coil*a^2 ./ (2*pi) * ( 1 ./ ( (a^2/4 + (x + q./2).^2) .* sqrt(a^2/2 + (x + q./2).^2)) + 1 ./ ( (a^2/4 + (x - q./2).^2) .* sqrt(a^2/2 + (x - q./2).^2))); # Gauss

x_c = 0;

[V,R] = voltage_calc(turns_in_coil,a,d,current_in_coil);
center_field = 100000 * mu_0*current_in_coil*turns_in_coil*a^2 ./ (2*pi) * ( 1 ./ ( (a^2/4 + (x_c + spacing./2).^2) .* sqrt(a^2/2 + (x_c + spacing./2).^2)) + 1 ./ ( (a^2/4 + (x_c - spacing./2).^2) .* sqrt(a^2/2 + (x_c - spacing./2).^2))); # Gauss
fprintf("The magnetic field in the center: %f uT\n",100*center_field )
fprintf("The required voltage: %f V \n",V )
## Plots
figure(1);
plot(x,100*field_intensity,'LineWidth',1.5);
grid on
xlabel('x(m)')
ylabel('uT')