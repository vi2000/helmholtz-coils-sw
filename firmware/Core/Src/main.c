/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  * @attention
  *
  * Copyright (c) 2023 STMicroelectronics.
  * All rights reserved.
  *
  * This software is licensed under terms that can be found in the LICENSE file
  * in the root directory of this software component.
  * If no LICENSE file comes with this software, it is provided AS-IS.
  *
  ******************************************************************************
  */
/* USER CODE END Header */
/* Includes ------------------------------------------------------------------*/
#include "main.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include "RM3100.h"
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
#include <stdbool.h>
/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */

#define MAX_RECEIVED_LENGTH 29
#define DO_CALIBRATION false
#define TEST_ACCURACY true
#define CALIB_STEP 0.02
#define SIM_SAT true


/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/
ADC_HandleTypeDef hadc1;

I2C_HandleTypeDef hi2c1;

TIM_HandleTypeDef htim1;
TIM_HandleTypeDef htim2;
TIM_HandleTypeDef htim3;

UART_HandleTypeDef huart1;
UART_HandleTypeDef huart2;
DMA_HandleTypeDef hdma_usart2_rx;

/* USER CODE BEGIN PV */
HAL_StatusTypeDef status;
RM3100 mag;
bool RECEIVED;
char msg[600];
char mag_msg[41];

uint8_t rx_buff[MAX_RECEIVED_LENGTH];
//uint8_t tx_buff[]={65,66,67,68,69,70,71,72,73,74}; //ABCDEFGHIJ in ASCII code

float duty_cycle[6] = {0.92,1.00,0.83,1.00,0.87,1.00}; // Declare the float array for duty cycle values
//float duty_cycle[6];
char receivedString[MAX_RECEIVED_LENGTH + 1]; // +1 for null terminator


/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
static void MX_GPIO_Init(void);
static void MX_DMA_Init(void);
static void MX_USART2_UART_Init(void);
static void MX_I2C1_Init(void);
static void MX_TIM1_Init(void);
static void MX_TIM2_Init(void);
static void MX_TIM3_Init(void);
static void MX_ADC1_Init(void);
static void MX_USART1_UART_Init(void);
/* USER CODE BEGIN PFP */

HAL_StatusTypeDef Helmholtz_cage_calibration(int NUMBER_OF_COMBINATIONS, HAL_StatusTypeDef status, char msg[]);


/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */

/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  * @retval int
  */
int main(void)
{
  /* USER CODE BEGIN 1 */


  /* USER CODE END 1 */

  /* MCU Configuration--------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */

  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_DMA_Init();
  MX_USART2_UART_Init();
  MX_I2C1_Init();
  MX_TIM1_Init();
  MX_TIM2_Init();
  MX_TIM3_Init();
  MX_ADC1_Init();
  MX_USART1_UART_Init();
  /* USER CODE BEGIN 2 */

  RM3100_Initialise(&mag,&hi2c1);
  HAL_UART_Receive_DMA(&huart2, rx_buff, MAX_RECEIVED_LENGTH);

  if (DO_CALIBRATION == true){
    int  NUMBER_OF_COMBINATIONS = 8*(1/CALIB_STEP + 1)*(1/CALIB_STEP + 1)*(1/CALIB_STEP + 1);
    char msg[600];
    status = Helmholtz_cage_calibration(NUMBER_OF_COMBINATIONS,status,msg);
  }


  /* USER CODE END 2 */

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */

  while (1)
  {

	  HAL_Delay(50);
    /* USER CODE END WHILE */

    /* USER CODE BEGIN 3 */

  }

  /* USER CODE END 3 */
}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};

  /** Configure the main internal regulator output voltage
  */
  if (HAL_PWREx_ControlVoltageScaling(PWR_REGULATOR_VOLTAGE_SCALE1) != HAL_OK)
  {
    Error_Handler();
  }

  /** Initializes the RCC Oscillators according to the specified parameters
  * in the RCC_OscInitTypeDef structure.
  */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSI;
  RCC_OscInitStruct.HSIState = RCC_HSI_ON;
  RCC_OscInitStruct.HSICalibrationValue = RCC_HSICALIBRATION_DEFAULT;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSI;
  RCC_OscInitStruct.PLL.PLLM = 1;
  RCC_OscInitStruct.PLL.PLLN = 10;
  RCC_OscInitStruct.PLL.PLLP = RCC_PLLP_DIV7;
  RCC_OscInitStruct.PLL.PLLQ = RCC_PLLQ_DIV2;
  RCC_OscInitStruct.PLL.PLLR = RCC_PLLR_DIV2;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }

  /** Initializes the CPU, AHB and APB buses clocks
  */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV1;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_4) != HAL_OK)
  {
    Error_Handler();
  }
}

/**
  * @brief ADC1 Initialization Function
  * @param None
  * @retval None
  */
static void MX_ADC1_Init(void)
{

  /* USER CODE BEGIN ADC1_Init 0 */

  /* USER CODE END ADC1_Init 0 */

  ADC_MultiModeTypeDef multimode = {0};
  ADC_ChannelConfTypeDef sConfig = {0};

  /* USER CODE BEGIN ADC1_Init 1 */

  /* USER CODE END ADC1_Init 1 */

  /** Common config
  */
  hadc1.Instance = ADC1;
  hadc1.Init.ClockPrescaler = ADC_CLOCK_ASYNC_DIV1;
  hadc1.Init.Resolution = ADC_RESOLUTION_12B;
  hadc1.Init.DataAlign = ADC_DATAALIGN_RIGHT;
  hadc1.Init.ScanConvMode = ADC_SCAN_ENABLE;
  hadc1.Init.EOCSelection = ADC_EOC_SINGLE_CONV;
  hadc1.Init.LowPowerAutoWait = DISABLE;
  hadc1.Init.ContinuousConvMode = DISABLE;
  hadc1.Init.NbrOfConversion = 3;
  hadc1.Init.DiscontinuousConvMode = DISABLE;
  hadc1.Init.ExternalTrigConv = ADC_SOFTWARE_START;
  hadc1.Init.ExternalTrigConvEdge = ADC_EXTERNALTRIGCONVEDGE_NONE;
  hadc1.Init.DMAContinuousRequests = DISABLE;
  hadc1.Init.Overrun = ADC_OVR_DATA_PRESERVED;
  hadc1.Init.OversamplingMode = DISABLE;
  if (HAL_ADC_Init(&hadc1) != HAL_OK)
  {
    Error_Handler();
  }

  /** Configure the ADC multi-mode
  */
  multimode.Mode = ADC_MODE_INDEPENDENT;
  if (HAL_ADCEx_MultiModeConfigChannel(&hadc1, &multimode) != HAL_OK)
  {
    Error_Handler();
  }

  /** Configure Regular Channel
  */
  sConfig.Channel = ADC_CHANNEL_15;
  sConfig.Rank = ADC_REGULAR_RANK_1;
  sConfig.SamplingTime = ADC_SAMPLETIME_2CYCLES_5;
  sConfig.SingleDiff = ADC_SINGLE_ENDED;
  sConfig.OffsetNumber = ADC_OFFSET_NONE;
  sConfig.Offset = 0;
  if (HAL_ADC_ConfigChannel(&hadc1, &sConfig) != HAL_OK)
  {
    Error_Handler();
  }

  /** Configure Regular Channel
  */
  sConfig.Channel = ADC_CHANNEL_3;
  sConfig.Rank = ADC_REGULAR_RANK_2;
  if (HAL_ADC_ConfigChannel(&hadc1, &sConfig) != HAL_OK)
  {
    Error_Handler();
  }

  /** Configure Regular Channel
  */
  sConfig.Channel = ADC_CHANNEL_4;
  sConfig.Rank = ADC_REGULAR_RANK_3;
  if (HAL_ADC_ConfigChannel(&hadc1, &sConfig) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN ADC1_Init 2 */

  /* USER CODE END ADC1_Init 2 */

}

/**
  * @brief I2C1 Initialization Function
  * @param None
  * @retval None
  */
static void MX_I2C1_Init(void)
{

  /* USER CODE BEGIN I2C1_Init 0 */

  /* USER CODE END I2C1_Init 0 */

  /* USER CODE BEGIN I2C1_Init 1 */

  /* USER CODE END I2C1_Init 1 */
  hi2c1.Instance = I2C1;
  hi2c1.Init.Timing = 0x00303D5B;
  hi2c1.Init.OwnAddress1 = 0;
  hi2c1.Init.AddressingMode = I2C_ADDRESSINGMODE_7BIT;
  hi2c1.Init.DualAddressMode = I2C_DUALADDRESS_DISABLE;
  hi2c1.Init.OwnAddress2 = 0;
  hi2c1.Init.OwnAddress2Masks = I2C_OA2_NOMASK;
  hi2c1.Init.GeneralCallMode = I2C_GENERALCALL_DISABLE;
  hi2c1.Init.NoStretchMode = I2C_NOSTRETCH_DISABLE;
  if (HAL_I2C_Init(&hi2c1) != HAL_OK)
  {
    Error_Handler();
  }

  /** Configure Analogue filter
  */
  if (HAL_I2CEx_ConfigAnalogFilter(&hi2c1, I2C_ANALOGFILTER_ENABLE) != HAL_OK)
  {
    Error_Handler();
  }

  /** Configure Digital filter
  */
  if (HAL_I2CEx_ConfigDigitalFilter(&hi2c1, 0) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN I2C1_Init 2 */

  /* USER CODE END I2C1_Init 2 */

}

/**
  * @brief TIM1 Initialization Function
  * @param None
  * @retval None
  */
static void MX_TIM1_Init(void)
{

  /* USER CODE BEGIN TIM1_Init 0 */

  /* USER CODE END TIM1_Init 0 */

  TIM_ClockConfigTypeDef sClockSourceConfig = {0};
  TIM_MasterConfigTypeDef sMasterConfig = {0};
  TIM_OC_InitTypeDef sConfigOC = {0};
  TIM_BreakDeadTimeConfigTypeDef sBreakDeadTimeConfig = {0};

  /* USER CODE BEGIN TIM1_Init 1 */

  /* USER CODE END TIM1_Init 1 */
  htim1.Instance = TIM1;
  htim1.Init.Prescaler = 0;
  htim1.Init.CounterMode = TIM_COUNTERMODE_UP;
  htim1.Init.Period = 8000-1;
  htim1.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
  htim1.Init.RepetitionCounter = 0;
  htim1.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_ENABLE;
  if (HAL_TIM_Base_Init(&htim1) != HAL_OK)
  {
    Error_Handler();
  }
  sClockSourceConfig.ClockSource = TIM_CLOCKSOURCE_INTERNAL;
  if (HAL_TIM_ConfigClockSource(&htim1, &sClockSourceConfig) != HAL_OK)
  {
    Error_Handler();
  }
  if (HAL_TIM_PWM_Init(&htim1) != HAL_OK)
  {
    Error_Handler();
  }
  sMasterConfig.MasterOutputTrigger = TIM_TRGO_RESET;
  sMasterConfig.MasterOutputTrigger2 = TIM_TRGO2_RESET;
  sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
  if (HAL_TIMEx_MasterConfigSynchronization(&htim1, &sMasterConfig) != HAL_OK)
  {
    Error_Handler();
  }
  sConfigOC.OCMode = TIM_OCMODE_PWM1;
  sConfigOC.Pulse = 0;
  sConfigOC.OCPolarity = TIM_OCPOLARITY_HIGH;
  sConfigOC.OCNPolarity = TIM_OCNPOLARITY_HIGH;
  sConfigOC.OCFastMode = TIM_OCFAST_DISABLE;
  sConfigOC.OCIdleState = TIM_OCIDLESTATE_RESET;
  sConfigOC.OCNIdleState = TIM_OCNIDLESTATE_RESET;
  if (HAL_TIM_PWM_ConfigChannel(&htim1, &sConfigOC, TIM_CHANNEL_1) != HAL_OK)
  {
    Error_Handler();
  }
  if (HAL_TIM_PWM_ConfigChannel(&htim1, &sConfigOC, TIM_CHANNEL_2) != HAL_OK)
  {
    Error_Handler();
  }
  if (HAL_TIM_PWM_ConfigChannel(&htim1, &sConfigOC, TIM_CHANNEL_3) != HAL_OK)
  {
    Error_Handler();
  }
  sBreakDeadTimeConfig.OffStateRunMode = TIM_OSSR_DISABLE;
  sBreakDeadTimeConfig.OffStateIDLEMode = TIM_OSSI_DISABLE;
  sBreakDeadTimeConfig.LockLevel = TIM_LOCKLEVEL_OFF;
  sBreakDeadTimeConfig.DeadTime = 0;
  sBreakDeadTimeConfig.BreakState = TIM_BREAK_DISABLE;
  sBreakDeadTimeConfig.BreakPolarity = TIM_BREAKPOLARITY_HIGH;
  sBreakDeadTimeConfig.BreakFilter = 0;
  sBreakDeadTimeConfig.Break2State = TIM_BREAK2_DISABLE;
  sBreakDeadTimeConfig.Break2Polarity = TIM_BREAK2POLARITY_HIGH;
  sBreakDeadTimeConfig.Break2Filter = 0;
  sBreakDeadTimeConfig.AutomaticOutput = TIM_AUTOMATICOUTPUT_DISABLE;
  if (HAL_TIMEx_ConfigBreakDeadTime(&htim1, &sBreakDeadTimeConfig) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN TIM1_Init 2 */
  HAL_TIM_Base_Start(&htim1);
  HAL_TIM_PWM_Start(&htim1, TIM_CHANNEL_1);
  HAL_TIM_PWM_Start(&htim1, TIM_CHANNEL_2);
  HAL_TIM_PWM_Start(&htim1, TIM_CHANNEL_3);
  /* USER CODE END TIM1_Init 2 */
  HAL_TIM_MspPostInit(&htim1);

}

/**
  * @brief TIM2 Initialization Function
  * @param None
  * @retval None
  */
static void MX_TIM2_Init(void)
{

  /* USER CODE BEGIN TIM2_Init 0 */

  /* USER CODE END TIM2_Init 0 */

  TIM_ClockConfigTypeDef sClockSourceConfig = {0};
  TIM_MasterConfigTypeDef sMasterConfig = {0};
  TIM_OC_InitTypeDef sConfigOC = {0};

  /* USER CODE BEGIN TIM2_Init 1 */

  /* USER CODE END TIM2_Init 1 */
  htim2.Instance = TIM2;
  htim2.Init.Prescaler = 0;
  htim2.Init.CounterMode = TIM_COUNTERMODE_UP;
  htim2.Init.Period = 8000-1;
  htim2.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
  htim2.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_ENABLE;
  if (HAL_TIM_Base_Init(&htim2) != HAL_OK)
  {
    Error_Handler();
  }
  sClockSourceConfig.ClockSource = TIM_CLOCKSOURCE_INTERNAL;
  if (HAL_TIM_ConfigClockSource(&htim2, &sClockSourceConfig) != HAL_OK)
  {
    Error_Handler();
  }
  if (HAL_TIM_PWM_Init(&htim2) != HAL_OK)
  {
    Error_Handler();
  }
  sMasterConfig.MasterOutputTrigger = TIM_TRGO_RESET;
  sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
  if (HAL_TIMEx_MasterConfigSynchronization(&htim2, &sMasterConfig) != HAL_OK)
  {
    Error_Handler();
  }
  sConfigOC.OCMode = TIM_OCMODE_PWM1;
  sConfigOC.Pulse = 0;
  sConfigOC.OCPolarity = TIM_OCPOLARITY_HIGH;
  sConfigOC.OCFastMode = TIM_OCFAST_DISABLE;
  if (HAL_TIM_PWM_ConfigChannel(&htim2, &sConfigOC, TIM_CHANNEL_1) != HAL_OK)
  {
    Error_Handler();
  }
  if (HAL_TIM_PWM_ConfigChannel(&htim2, &sConfigOC, TIM_CHANNEL_4) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN TIM2_Init 2 */
  HAL_TIM_Base_Start(&htim2);
  HAL_TIM_PWM_Start(&htim2, TIM_CHANNEL_4);
  /* USER CODE END TIM2_Init 2 */
  HAL_TIM_MspPostInit(&htim2);

}

/**
  * @brief TIM3 Initialization Function
  * @param None
  * @retval None
  */
static void MX_TIM3_Init(void)
{

  /* USER CODE BEGIN TIM3_Init 0 */


  /* USER CODE END TIM3_Init 0 */

  TIM_ClockConfigTypeDef sClockSourceConfig = {0};
  TIM_MasterConfigTypeDef sMasterConfig = {0};
  TIM_OC_InitTypeDef sConfigOC = {0};

  /* USER CODE BEGIN TIM3_Init 1 */

  /* USER CODE END TIM3_Init 1 */
  htim3.Instance = TIM3;
  htim3.Init.Prescaler = 0;
  htim3.Init.CounterMode = TIM_COUNTERMODE_UP;
  htim3.Init.Period = 8000-1;
  htim3.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
  htim3.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_ENABLE;
  if (HAL_TIM_Base_Init(&htim3) != HAL_OK)
  {
    Error_Handler();
  }
  sClockSourceConfig.ClockSource = TIM_CLOCKSOURCE_INTERNAL;
  if (HAL_TIM_ConfigClockSource(&htim3, &sClockSourceConfig) != HAL_OK)
  {
    Error_Handler();
  }
  if (HAL_TIM_PWM_Init(&htim3) != HAL_OK)
  {
    Error_Handler();
  }
  sMasterConfig.MasterOutputTrigger = TIM_TRGO_RESET;
  sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
  if (HAL_TIMEx_MasterConfigSynchronization(&htim3, &sMasterConfig) != HAL_OK)
  {
    Error_Handler();
  }
  sConfigOC.OCMode = TIM_OCMODE_PWM1;
  sConfigOC.Pulse = 0;
  sConfigOC.OCPolarity = TIM_OCPOLARITY_HIGH;
  sConfigOC.OCFastMode = TIM_OCFAST_DISABLE;
  if (HAL_TIM_PWM_ConfigChannel(&htim3, &sConfigOC, TIM_CHANNEL_1) != HAL_OK)
  {
    Error_Handler();
  }
  if (HAL_TIM_PWM_ConfigChannel(&htim3, &sConfigOC, TIM_CHANNEL_2) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN TIM3_Init 2 */
  HAL_TIM_Base_Start(&htim3);
  HAL_TIM_PWM_Start(&htim3, TIM_CHANNEL_1);
  HAL_TIM_PWM_Start(&htim3, TIM_CHANNEL_2);
  /* USER CODE END TIM3_Init 2 */
  HAL_TIM_MspPostInit(&htim3);

}

/**
  * @brief USART1 Initialization Function
  * @param None
  * @retval None
  */
static void MX_USART1_UART_Init(void)
{

  /* USER CODE BEGIN USART1_Init 0 */

  /* USER CODE END USART1_Init 0 */

  /* USER CODE BEGIN USART1_Init 1 */

  /* USER CODE END USART1_Init 1 */
  huart1.Instance = USART1;
  huart1.Init.BaudRate = 115200;
  huart1.Init.WordLength = UART_WORDLENGTH_8B;
  huart1.Init.StopBits = UART_STOPBITS_1;
  huart1.Init.Parity = UART_PARITY_NONE;
  huart1.Init.Mode = UART_MODE_TX_RX;
  huart1.Init.HwFlowCtl = UART_HWCONTROL_NONE;
  huart1.Init.OverSampling = UART_OVERSAMPLING_16;
  huart1.Init.OneBitSampling = UART_ONE_BIT_SAMPLE_DISABLE;
  huart1.AdvancedInit.AdvFeatureInit = UART_ADVFEATURE_NO_INIT;
  if (HAL_UART_Init(&huart1) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN USART1_Init 2 */

  /* USER CODE END USART1_Init 2 */

}

/**
  * @brief USART2 Initialization Function
  * @param None
  * @retval None
  */
static void MX_USART2_UART_Init(void)
{

  /* USER CODE BEGIN USART2_Init 0 */

  /* USER CODE END USART2_Init 0 */

  /* USER CODE BEGIN USART2_Init 1 */

  /* USER CODE END USART2_Init 1 */
  huart2.Instance = USART2;
  huart2.Init.BaudRate = 115200;
  huart2.Init.WordLength = UART_WORDLENGTH_8B;
  huart2.Init.StopBits = UART_STOPBITS_1;
  huart2.Init.Parity = UART_PARITY_NONE;
  huart2.Init.Mode = UART_MODE_TX_RX;
  huart2.Init.HwFlowCtl = UART_HWCONTROL_NONE;
  huart2.Init.OverSampling = UART_OVERSAMPLING_16;
  huart2.Init.OneBitSampling = UART_ONE_BIT_SAMPLE_DISABLE;
  huart2.AdvancedInit.AdvFeatureInit = UART_ADVFEATURE_NO_INIT;
  if (HAL_UART_Init(&huart2) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN USART2_Init 2 */

  /* USER CODE END USART2_Init 2 */

}

/**
  * Enable DMA controller clock
  */
static void MX_DMA_Init(void)
{

  /* DMA controller clock enable */
  __HAL_RCC_DMA1_CLK_ENABLE();

  /* DMA interrupt init */
  /* DMA1_Channel6_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(DMA1_Channel6_IRQn, 0, 0);
  HAL_NVIC_EnableIRQ(DMA1_Channel6_IRQn);

}

/**
  * @brief GPIO Initialization Function
  * @param None
  * @retval None
  */
static void MX_GPIO_Init(void)
{
  GPIO_InitTypeDef GPIO_InitStruct = {0};
/* USER CODE BEGIN MX_GPIO_Init_1 */
/* USER CODE END MX_GPIO_Init_1 */

  /* GPIO Ports Clock Enable */
  __HAL_RCC_GPIOC_CLK_ENABLE();
  __HAL_RCC_GPIOH_CLK_ENABLE();
  __HAL_RCC_GPIOA_CLK_ENABLE();
  __HAL_RCC_GPIOB_CLK_ENABLE();

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(LD2_GPIO_Port, LD2_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOB, GPIO_PIN_10, GPIO_PIN_RESET);

  /*Configure GPIO pin : B1_Pin */
  GPIO_InitStruct.Pin = B1_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_IT_FALLING;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(B1_GPIO_Port, &GPIO_InitStruct);

  /*Configure GPIO pin : LD2_Pin */
  GPIO_InitStruct.Pin = LD2_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(LD2_GPIO_Port, &GPIO_InitStruct);

  /*Configure GPIO pin : PB10 */
  GPIO_InitStruct.Pin = GPIO_PIN_10;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

/* USER CODE BEGIN MX_GPIO_Init_2 */
/* USER CODE END MX_GPIO_Init_2 */
}

/* USER CODE BEGIN 4 */

void HAL_UART_RxCpltCallback(UART_HandleTypeDef *huart2)
{

    			strncpy(receivedString, (char*) rx_buff, MAX_RECEIVED_LENGTH);
    			//receivedString[MAX_RECEIVED_LENGTH] = '\0'; // Null-terminate the string

    			char firstDigit = receivedString[0];
    			int stringLength = strlen(receivedString);

//    			memmove(receivedString, receivedString + 1, stringLength); // Remove the first digit
//    			receivedString[stringLength - 1] = firstDigit; // Append the first digit to the end

    			// Use strtok to split the received data into individual tokens
    			char *token = strtok(receivedString, ",");
    			int index = 0;
    			// Loop through tokens and convert to floats using atof
    			while (token != NULL && index < 9) {
    				// Convert token to float and store in appropriate array
    				if (index < 6) {
    					duty_cycle[index] = atof(token);
    				}
    				index++;

    				// Get the next token
    				token = strtok(NULL, ",");

    			}


    	  float diff0 = fabs(duty_cycle[0] - 1);
          float diff1 = fabs(duty_cycle[1] - 1);
          float diff2 = fabs(duty_cycle[2] - 1);
          float diff3 = fabs(duty_cycle[3] - 1);
          float diff4 = fabs(duty_cycle[4] - 1);
          float diff5 = fabs(duty_cycle[5] - 1);


          if (diff0 < diff1) {
            __HAL_TIM_SET_COMPARE(&htim1, TIM_CHANNEL_1, (htim1.Init.Period + 1)); // AXIS Z, when == 1 **negative** magnetic field generation
            __HAL_TIM_SET_COMPARE(&htim1, TIM_CHANNEL_2, (htim1.Init.Period + 1)*duty_cycle[1]); // when == 1 **positive* magnetic field generation

          } else if (diff0 > diff1){
            __HAL_TIM_SET_COMPARE(&htim1, TIM_CHANNEL_1, (htim1.Init.Period + 1)*duty_cycle[0]); // AXIS Z, when == 1 **negative** magnetic field generation
            __HAL_TIM_SET_COMPARE(&htim1, TIM_CHANNEL_2, (htim1.Init.Period + 1)); // when == 1 **positive* magnetic field generation
          } else {
            __HAL_TIM_SET_COMPARE(&htim1, TIM_CHANNEL_1, (htim1.Init.Period + 1)); // AXIS Z, when == 1 **negative** magnetic field generation
            __HAL_TIM_SET_COMPARE(&htim1, TIM_CHANNEL_2, (htim1.Init.Period + 1)); // when == 1 **positive* magnetic field generation
          }



          if (diff2 < diff3) {
            __HAL_TIM_SET_COMPARE(&htim1, TIM_CHANNEL_3, (htim1.Init.Period + 1)); // AXIS Y, when == 1 **positive** magnetic field generation
            __HAL_TIM_SET_COMPARE(&htim2, TIM_CHANNEL_4, (htim2.Init.Period + 1)*duty_cycle[3]); // when == 1 **negative** magnetic field generation
          } else if (diff2 > diff3){
            __HAL_TIM_SET_COMPARE(&htim1, TIM_CHANNEL_3, (htim1.Init.Period + 1)*duty_cycle[2]); // AXIS Y, when == 1 **positive** magnetic field generation
            __HAL_TIM_SET_COMPARE(&htim2, TIM_CHANNEL_4, (htim2.Init.Period + 1)); // when == 1 **negative** magnetic field generation
          } else {
            __HAL_TIM_SET_COMPARE(&htim1, TIM_CHANNEL_3, (htim1.Init.Period + 1)); // AXIS Y, when == 1 **positive** magnetic field generation
            __HAL_TIM_SET_COMPARE(&htim2, TIM_CHANNEL_4, (htim2.Init.Period + 1)); // when == 1 **negative** magnetic field generation
          }




          if (diff4 < diff5) {
            __HAL_TIM_SET_COMPARE(&htim3, TIM_CHANNEL_1, (htim3.Init.Period + 1)); // AXIS X, when == 1 **positive** magnetic field generation
            __HAL_TIM_SET_COMPARE(&htim3, TIM_CHANNEL_2, (htim3.Init.Period + 1)*duty_cycle[5]); //when == 1 **negative** magnetic field generation
          } else if (diff4 > diff5){
            __HAL_TIM_SET_COMPARE(&htim3, TIM_CHANNEL_1, (htim3.Init.Period + 1)*duty_cycle[4]); // AXIS X, when == 1 **positive** magnetic field generation
            __HAL_TIM_SET_COMPARE(&htim3, TIM_CHANNEL_2, (htim3.Init.Period + 1)); //when == 1 **negative** magnetic field generation
          } else {
            __HAL_TIM_SET_COMPARE(&htim3, TIM_CHANNEL_1, (htim3.Init.Period + 1)); // AXIS X, when == 1 **positive** magnetic field generation
            __HAL_TIM_SET_COMPARE(&htim3, TIM_CHANNEL_2, (htim3.Init.Period + 1)); //when == 1 **negative** magnetic field generation
          }

////
//          __HAL_TIM_SET_COMPARE(&htim1, TIM_CHANNEL_1, (htim1.Init.Period + 1)*0.92); // AXIS Z, when == 1 **negative** magnetic field generation
//          __HAL_TIM_SET_COMPARE(&htim1, TIM_CHANNEL_2, (htim1.Init.Period + 1)); // when == 1 **positive* magnetic field generation
//
//
//          __HAL_TIM_SET_COMPARE(&htim1, TIM_CHANNEL_3, (htim1.Init.Period + 1)*0.83); // AXIS Y, when == 1 **positive** magnetic field generation
//          __HAL_TIM_SET_COMPARE(&htim2, TIM_CHANNEL_4, (htim2.Init.Period + 1)); // when == 1 **negative** magnetic field generation
//
//          __HAL_TIM_SET_COMPARE(&htim3, TIM_CHANNEL_1, (htim3.Init.Period + 1)*0.87); // AXIS X, when == 1 **positive** magnetic field generation
//          __HAL_TIM_SET_COMPARE(&htim3, TIM_CHANNEL_2, (htim3.Init.Period + 1)); //when == 1 **negative** magnetic field generation



    if (TEST_ACCURACY == true){
      status = RM3100_ReadMagneticField(&mag);
      sprintf(mag_msg, "%f, %f, %f \r\n", mag.mag_rm3100[0], mag.mag_rm3100[1], mag.mag_rm3100[2]);
//      HAL_Delay(100);

//      HAL_UART_Transmit(huart2, rx_buff, MAX_RECEIVED_LENGTH,100);
      HAL_UART_Transmit(huart2, (uint8_t*)mag_msg, strlen(mag_msg), HAL_MAX_DELAY); // UART TRANSMITION takes 1.04 ms
      HAL_UART_Receive_DMA(&huart2, rx_buff, MAX_RECEIVED_LENGTH);
      HAL_GPIO_TogglePin(LD2_GPIO_Port,LD2_Pin);
  }

}

HAL_StatusTypeDef Helmholtz_cage_calibration(int NUMBER_OF_COMBINATIONS, HAL_StatusTypeDef status,char msg[])
{

	int count = 0;
	float duty_cycle[6] = {0.0, 0.0, 0.0, 0.0, 0.0, 0.0}; // 0.7 == 30% duty cycle


    /* ------------------ START MESSAGE  --------------------*/
	for (int i = 0; i <= 5; i++) {
		sprintf(msg, "Starting Calibration \r\n");
		HAL_UART_Transmit(&huart2, (uint8_t*)msg, strlen(msg), HAL_MAX_DELAY); // UART TRANSMITION takes 1.04 ms
	}


    /* ------------------ POSITIVE CASE 1/4  --------------------*/

	duty_cycle[1] = 1;
	duty_cycle[3] = 1;
	duty_cycle[5] = 1;

	for (duty_cycle[0] = 0; duty_cycle[0] < 1.04; duty_cycle[0] = duty_cycle[0] + CALIB_STEP) {
		for (duty_cycle[2] = 0; duty_cycle[2] < 1.04; duty_cycle[2] = duty_cycle[2] + CALIB_STEP) {
			for (duty_cycle[4] = 0; duty_cycle[4] < 1.04; duty_cycle[4] = duty_cycle[4] + CALIB_STEP) {


			   count++;

				/* COIL 1 */
				__HAL_TIM_SET_COMPARE(&htim1, TIM_CHANNEL_1, (htim1.Init.Period + 1)*duty_cycle[0]); //bottom
				__HAL_TIM_SET_COMPARE(&htim1, TIM_CHANNEL_2, (htim1.Init.Period + 1)*duty_cycle[1]);
				/* COIL 2 */
				__HAL_TIM_SET_COMPARE(&htim1, TIM_CHANNEL_3, (htim1.Init.Period + 1)*duty_cycle[2]); // middle
				__HAL_TIM_SET_COMPARE(&htim2, TIM_CHANNEL_4, (htim2.Init.Period + 1)*duty_cycle[3]);
				/* COIL 3 */
				__HAL_TIM_SET_COMPARE(&htim3, TIM_CHANNEL_1, (htim3.Init.Period + 1)*duty_cycle[4]); // top
				__HAL_TIM_SET_COMPARE(&htim3, TIM_CHANNEL_2, (htim3.Init.Period + 1)*duty_cycle[5]);

				/* ------------------ READ MAGNETIC FIELD AND LOG DATA --------------------*/
				for (int i = 0; i < 3; i++) {
				   status = RM3100_ReadMagneticField(&mag);
						sprintf(msg, "Completion %d/%d, %f, %f, %f, %f, %f, %f, %f, %f, %f \r\n", count, NUMBER_OF_COMBINATIONS, duty_cycle[0],duty_cycle[1],duty_cycle[2],duty_cycle[3],duty_cycle[4],duty_cycle[5],mag.mag_rm3100[0], mag.mag_rm3100[1], mag.mag_rm3100[2]);

					   HAL_UART_Transmit(&huart2, (uint8_t*)msg, strlen(msg), HAL_MAX_DELAY); // UART TRANSMITION takes 1.04 ms
					  // The next measurement is ready in approximately 1.75 ms ± 0.1225ms
				}
			}
		}
	}





	/* ------------------ POSITIVE CASE 2/4  --------------------*/

	duty_cycle[1] = 1;
	duty_cycle[2] = 1;
	duty_cycle[5] = 1;

	for (duty_cycle[0] = 0; duty_cycle[0] < 1.04; duty_cycle[0] = duty_cycle[0] + CALIB_STEP) {
		for (duty_cycle[3] = 0; duty_cycle[3] < 1.04; duty_cycle[3] = duty_cycle[3] + CALIB_STEP) {
			for (duty_cycle[4] = 0; duty_cycle[4] < 1.04; duty_cycle[4] = duty_cycle[4] + CALIB_STEP) {


			   count++;

				/* COIL 1 */
				__HAL_TIM_SET_COMPARE(&htim1, TIM_CHANNEL_1, (htim1.Init.Period + 1)*duty_cycle[0]); //bottom
				__HAL_TIM_SET_COMPARE(&htim1, TIM_CHANNEL_2, (htim1.Init.Period + 1)*duty_cycle[1]);
				/* COIL 2 */
				__HAL_TIM_SET_COMPARE(&htim1, TIM_CHANNEL_3, (htim1.Init.Period + 1)*duty_cycle[2]); // middle
				__HAL_TIM_SET_COMPARE(&htim2, TIM_CHANNEL_4, (htim2.Init.Period + 1)*duty_cycle[3]);
				/* COIL 3 */
				__HAL_TIM_SET_COMPARE(&htim3, TIM_CHANNEL_1, (htim3.Init.Period + 1)*duty_cycle[4]); // top
				__HAL_TIM_SET_COMPARE(&htim3, TIM_CHANNEL_2, (htim3.Init.Period + 1)*duty_cycle[5]);

				/* ------------------ READ MAGNETIC FIELD AND LOG DATA --------------------*/
				for (int i = 0; i < 3; i++) {
				   status = RM3100_ReadMagneticField(&mag);
						sprintf(msg, "Completion %d/%d, %f, %f, %f, %f, %f, %f, %f, %f, %f \r\n", count, NUMBER_OF_COMBINATIONS, duty_cycle[0],duty_cycle[1],duty_cycle[2],duty_cycle[3],duty_cycle[4],duty_cycle[5],mag.mag_rm3100[0], mag.mag_rm3100[1], mag.mag_rm3100[2]);

					   HAL_UART_Transmit(&huart2, (uint8_t*)msg, strlen(msg), HAL_MAX_DELAY); // UART TRANSMITION takes 1.04 ms
					  // The next measurement is ready in approximately 1.75 ms ± 0.1225ms
				}

			}
		}

	}

	/* ------------------ POSITIVE CASE 3/4  --------------------*/

	duty_cycle[1] = 1;
	duty_cycle[3] = 1;
	duty_cycle[4] = 1;

	for (duty_cycle[0] = 0; duty_cycle[0] < 1.04; duty_cycle[0] = duty_cycle[0] + CALIB_STEP) {
		for (duty_cycle[2] = 0; duty_cycle[2] < 1.04; duty_cycle[2] = duty_cycle[2] + CALIB_STEP) {
			for (duty_cycle[5] = 0; duty_cycle[5] < 1.04; duty_cycle[5] = duty_cycle[5] + CALIB_STEP) {


				count++;

				/* COIL 1 */
				__HAL_TIM_SET_COMPARE(&htim1, TIM_CHANNEL_1, (htim1.Init.Period + 1)*duty_cycle[0]); //bottom
				__HAL_TIM_SET_COMPARE(&htim1, TIM_CHANNEL_2, (htim1.Init.Period + 1)*duty_cycle[1]);
				/* COIL 2 */
				__HAL_TIM_SET_COMPARE(&htim1, TIM_CHANNEL_3, (htim1.Init.Period + 1)*duty_cycle[2]); // middle
				__HAL_TIM_SET_COMPARE(&htim2, TIM_CHANNEL_4, (htim2.Init.Period + 1)*duty_cycle[3]);
				/* COIL 3 */
				__HAL_TIM_SET_COMPARE(&htim3, TIM_CHANNEL_1, (htim3.Init.Period + 1)*duty_cycle[4]); // top
				__HAL_TIM_SET_COMPARE(&htim3, TIM_CHANNEL_2, (htim3.Init.Period + 1)*duty_cycle[5]);

				/* ------------------ READ MAGNETIC FIELD AND LOG DATA --------------------*/
				for (int i = 0; i < 3; i++) {
				status = RM3100_ReadMagneticField(&mag);
				sprintf(msg, "Completion %d/%d, %f, %f, %f, %f, %f, %f, %f, %f, %f \r\n", count, NUMBER_OF_COMBINATIONS, duty_cycle[0],duty_cycle[1],duty_cycle[2],duty_cycle[3],duty_cycle[4],duty_cycle[5],mag.mag_rm3100[0], mag.mag_rm3100[1], mag.mag_rm3100[2]);

				HAL_UART_Transmit(&huart2, (uint8_t*)msg, strlen(msg), HAL_MAX_DELAY); // UART TRANSMITION takes 1.04 ms
				// The next measurement is ready in approximately 1.75 ms ± 0.1225ms

				}
			}
		}
	}

	/* ------------------ POSITIVE CASE 4/4  --------------------*/


	duty_cycle[1] = 1;
	duty_cycle[2] = 1;
	duty_cycle[4] = 1;

	for (duty_cycle[0] = 0; duty_cycle[0] < 1.04; duty_cycle[0] = duty_cycle[0] + CALIB_STEP) {
		for (duty_cycle[3] = 0; duty_cycle[3] < 1.04; duty_cycle[3] = duty_cycle[3] + CALIB_STEP) {
			for (duty_cycle[5] = 0; duty_cycle[5] < 1.04; duty_cycle[5] = duty_cycle[5] + CALIB_STEP) {


				count++;

				/* COIL 1 */
				__HAL_TIM_SET_COMPARE(&htim1, TIM_CHANNEL_1, (htim1.Init.Period + 1)*duty_cycle[0]); //bottom
				__HAL_TIM_SET_COMPARE(&htim1, TIM_CHANNEL_2, (htim1.Init.Period + 1)*duty_cycle[1]);
				/* COIL 2 */
				__HAL_TIM_SET_COMPARE(&htim1, TIM_CHANNEL_3, (htim1.Init.Period + 1)*duty_cycle[2]); // middle
				__HAL_TIM_SET_COMPARE(&htim2, TIM_CHANNEL_4, (htim2.Init.Period + 1)*duty_cycle[3]);
				/* COIL 3 */
				__HAL_TIM_SET_COMPARE(&htim3, TIM_CHANNEL_1, (htim3.Init.Period + 1)*duty_cycle[4]); // top
				__HAL_TIM_SET_COMPARE(&htim3, TIM_CHANNEL_2, (htim3.Init.Period + 1)*duty_cycle[5]);

				/* ------------------ READ MAGNETIC FIELD AND LOG DATA --------------------*/
				for (int i = 0; i < 3; i++) {
				status = RM3100_ReadMagneticField(&mag);
				sprintf(msg, "Completion %d/%d, %f, %f, %f, %f, %f, %f, %f, %f, %f \r\n", count, NUMBER_OF_COMBINATIONS, duty_cycle[0],duty_cycle[1],duty_cycle[2],duty_cycle[3],duty_cycle[4],duty_cycle[5],mag.mag_rm3100[0], mag.mag_rm3100[1], mag.mag_rm3100[2]);

				HAL_UART_Transmit(&huart2, (uint8_t*)msg, strlen(msg), HAL_MAX_DELAY); // UART TRANSMITION takes 1.04 ms
				// The next measurement is ready in approximately 1.75 ms ± 0.1225ms

				}
			}
		}
	}


    /* ------------------ NEGATIVE CASE 1/4  --------------------*/

	duty_cycle[0] = 1;
	duty_cycle[3] = 1;
	duty_cycle[5] = 1;

	for (duty_cycle[1] = 0; duty_cycle[1] < 1.04; duty_cycle[1] = duty_cycle[1] + CALIB_STEP) {
		for (duty_cycle[2] = 0; duty_cycle[2] < 1.04; duty_cycle[2] = duty_cycle[2] + CALIB_STEP) {
			for (duty_cycle[4] = 0; duty_cycle[4] < 1.04; duty_cycle[4] = duty_cycle[4] + CALIB_STEP) {


			   count++;

				/* COIL 1 */
				__HAL_TIM_SET_COMPARE(&htim1, TIM_CHANNEL_1, (htim1.Init.Period + 1)*duty_cycle[0]); //bottom
				__HAL_TIM_SET_COMPARE(&htim1, TIM_CHANNEL_2, (htim1.Init.Period + 1)*duty_cycle[1]);
				/* COIL 2 */
				__HAL_TIM_SET_COMPARE(&htim1, TIM_CHANNEL_3, (htim1.Init.Period + 1)*duty_cycle[2]); // middle
				__HAL_TIM_SET_COMPARE(&htim2, TIM_CHANNEL_4, (htim2.Init.Period + 1)*duty_cycle[3]);
				/* COIL 3 */
				__HAL_TIM_SET_COMPARE(&htim3, TIM_CHANNEL_1, (htim3.Init.Period + 1)*duty_cycle[4]); // top
				__HAL_TIM_SET_COMPARE(&htim3, TIM_CHANNEL_2, (htim3.Init.Period + 1)*duty_cycle[5]);

				/* ------------------ READ MAGNETIC FIELD AND LOG DATA --------------------*/
				for (int i = 0; i <= 3; i++) {
				   status = RM3100_ReadMagneticField(&mag);
					   sprintf(msg, "Completion %d/%d, %f, %f, %f, %f, %f, %f, %f, %f, %f \r\n", count, NUMBER_OF_COMBINATIONS, duty_cycle[0],duty_cycle[1],duty_cycle[2],duty_cycle[3],duty_cycle[4],duty_cycle[5],mag.mag_rm3100[0], mag.mag_rm3100[1], mag.mag_rm3100[2]);

					   HAL_UART_Transmit(&huart2, (uint8_t*)msg, strlen(msg), HAL_MAX_DELAY); // UART TRANSMITION takes 1.04 ms
					  // The next measurement is ready in approximately 1.75 ms ± 0.1225ms
				}
			}
		}

	}





	/* ------------------ NAGATIVE CASE 2/4  --------------------*/

	duty_cycle[0] = 1;
	duty_cycle[2] = 1;
	duty_cycle[5] = 1;

	for (duty_cycle[1] = 0; duty_cycle[1] < 1.04; duty_cycle[1] = duty_cycle[1] + CALIB_STEP) {
		for (duty_cycle[3] = 0; duty_cycle[3] < 1.04; duty_cycle[3] = duty_cycle[3] + CALIB_STEP) {
			for (duty_cycle[4] = 0; duty_cycle[4] < 1.04; duty_cycle[4] = duty_cycle[4] + CALIB_STEP) {


			   count++;

				/* COIL 1 */
				__HAL_TIM_SET_COMPARE(&htim1, TIM_CHANNEL_1, (htim1.Init.Period + 1)*duty_cycle[0]); //bottom
				__HAL_TIM_SET_COMPARE(&htim1, TIM_CHANNEL_2, (htim1.Init.Period + 1)*duty_cycle[1]);
				/* COIL 2 */
				__HAL_TIM_SET_COMPARE(&htim1, TIM_CHANNEL_3, (htim1.Init.Period + 1)*duty_cycle[2]); // middle
				__HAL_TIM_SET_COMPARE(&htim2, TIM_CHANNEL_4, (htim2.Init.Period + 1)*duty_cycle[3]);
				/* COIL 3 */
				__HAL_TIM_SET_COMPARE(&htim3, TIM_CHANNEL_1, (htim3.Init.Period + 1)*duty_cycle[4]); // top
				__HAL_TIM_SET_COMPARE(&htim3, TIM_CHANNEL_2, (htim3.Init.Period + 1)*duty_cycle[5]);

				/* ------------------ READ MAGNETIC FIELD AND LOG DATA --------------------*/
				for (int i = 0; i < 3; i++) {
				   status = RM3100_ReadMagneticField(&mag);
						sprintf(msg, "Completion %d/%d, %f, %f, %f, %f, %f, %f, %f, %f, %f \r\n", count, NUMBER_OF_COMBINATIONS, duty_cycle[0],duty_cycle[1],duty_cycle[2],duty_cycle[3],duty_cycle[4],duty_cycle[5],mag.mag_rm3100[0], mag.mag_rm3100[1], mag.mag_rm3100[2]);

					   HAL_UART_Transmit(&huart2, (uint8_t*)msg, strlen(msg), HAL_MAX_DELAY); // UART TRANSMITION takes 1.04 ms
					  // The next measurement is ready in approximately 1.75 ms ± 0.1225ms
				}
			}
		}
	}

	/* ------------------ NAGATIVE CASE 3/4  --------------------*/

	duty_cycle[0] = 1;
	duty_cycle[3] = 1;
	duty_cycle[4] = 1;

	for (duty_cycle[1] = 0; duty_cycle[1] < 1.04; duty_cycle[1] = duty_cycle[1] + CALIB_STEP) {
		for (duty_cycle[2] = 0; duty_cycle[2] < 1.04; duty_cycle[2] = duty_cycle[2] + CALIB_STEP) {
			for (duty_cycle[5] = 0; duty_cycle[5] < 1.04; duty_cycle[5] = duty_cycle[5] + CALIB_STEP) {


				count++;

				/* COIL 1 */
				__HAL_TIM_SET_COMPARE(&htim1, TIM_CHANNEL_1, (htim1.Init.Period + 1)*duty_cycle[0]); //bottom
				__HAL_TIM_SET_COMPARE(&htim1, TIM_CHANNEL_2, (htim1.Init.Period + 1)*duty_cycle[1]);
				/* COIL 2 */
				__HAL_TIM_SET_COMPARE(&htim1, TIM_CHANNEL_3, (htim1.Init.Period + 1)*duty_cycle[2]); // middle
				__HAL_TIM_SET_COMPARE(&htim2, TIM_CHANNEL_4, (htim2.Init.Period + 1)*duty_cycle[3]);
				/* COIL 3 */
				__HAL_TIM_SET_COMPARE(&htim3, TIM_CHANNEL_1, (htim3.Init.Period + 1)*duty_cycle[4]); // top
				__HAL_TIM_SET_COMPARE(&htim3, TIM_CHANNEL_2, (htim3.Init.Period + 1)*duty_cycle[5]);

				/* ------------------ READ MAGNETIC FIELD AND LOG DATA --------------------*/
				for (int i = 0; i < 3; i++) {
				status = RM3100_ReadMagneticField(&mag);
				sprintf(msg, "Completion %d/%d, %f, %f, %f, %f, %f, %f, %f, %f, %f \r\n", count, NUMBER_OF_COMBINATIONS, duty_cycle[0],duty_cycle[1],duty_cycle[2],duty_cycle[3],duty_cycle[4],duty_cycle[5],mag.mag_rm3100[0], mag.mag_rm3100[1], mag.mag_rm3100[2]);
				HAL_UART_Transmit(&huart2, (uint8_t*)msg, strlen(msg), HAL_MAX_DELAY); // UART TRANSMITION takes 1.04 ms
				// The next measurement is ready in approximately 1.75 ms ± 0.1225ms

				}
			}
		}
	}

	/* ------------------ NAGATIVE CASE 4/4  --------------------*/


	duty_cycle[0] = 1;
	duty_cycle[2] = 1;
	duty_cycle[4] = 1;

	for (duty_cycle[1] = 0; duty_cycle[1] < 1.04; duty_cycle[1] = duty_cycle[1] + CALIB_STEP) {
		for (duty_cycle[3] = 0; duty_cycle[3] < 1.04; duty_cycle[3] = duty_cycle[3] + CALIB_STEP) {
			for (duty_cycle[5] = 0; duty_cycle[5] < 1.04; duty_cycle[5] = duty_cycle[5] + CALIB_STEP) {


				count++;

				/* COIL 1 */
				__HAL_TIM_SET_COMPARE(&htim1, TIM_CHANNEL_1, (htim1.Init.Period + 1)*duty_cycle[0]); //bottom
				__HAL_TIM_SET_COMPARE(&htim1, TIM_CHANNEL_2, (htim1.Init.Period + 1)*duty_cycle[1]);
				/* COIL 2 */
				__HAL_TIM_SET_COMPARE(&htim1, TIM_CHANNEL_3, (htim1.Init.Period + 1)*duty_cycle[2]); // middle
				__HAL_TIM_SET_COMPARE(&htim2, TIM_CHANNEL_4, (htim2.Init.Period + 1)*duty_cycle[3]);
				/* COIL 3 */
				__HAL_TIM_SET_COMPARE(&htim3, TIM_CHANNEL_1, (htim3.Init.Period + 1)*duty_cycle[4]); // top
				__HAL_TIM_SET_COMPARE(&htim3, TIM_CHANNEL_2, (htim3.Init.Period + 1)*duty_cycle[5]);

				/* ------------------ READ MAGNETIC FIELD AND LOG DATA --------------------*/
				for (int i = 0; i < 3; i++) {
				status = RM3100_ReadMagneticField(&mag);
				sprintf(msg, "Completion %d/%d, %f, %f, %f, %f, %f, %f, %f, %f, %f \r\n", count, NUMBER_OF_COMBINATIONS, duty_cycle[0],duty_cycle[1],duty_cycle[2],duty_cycle[3],duty_cycle[4],duty_cycle[5],mag.mag_rm3100[0], mag.mag_rm3100[1], mag.mag_rm3100[2]);

				HAL_UART_Transmit(&huart2, (uint8_t*)msg, strlen(msg), HAL_MAX_DELAY); // UART TRANSMITION takes 1.04 ms
				// The next measurement is ready in approximately 1.75 ms ± 0.1225ms

				}
			}
		}
	}





	return status;
}


/*
void HAL_ADC_ConvCpltCallback(ADC_HandleTypeDef *hadc)
{

	//adcConversionComplete = 1;

 NOTE : This function should not be modified. When the callback is needed,
            function HAL_ADC_ConvCpltCallback must be implemented in the user file.
   */

//}



/* USER CODE END 4 */

/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */
  __disable_irq();
  while (1)
  {
  }
  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */
